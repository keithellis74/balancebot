/*
BalanceBot Arduino code, BalanceBot is an inverted pendulum robot powered by DC
motors and keeps itself upright.  Also recieved commands over the serial UART to
 move.  Heavily modified the code from Joop Brokking http://www.brokking.net/yabr_main.html
Copyright (C) 2018  Keith Ellis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <Wire.h>                                            //Include the Wire.h library so we can communicate with the gyro

int gyro_address = 0x68;                                     //MPU-6050 I2C address (0x68 or 0x69)
/*
 * 1469 - Lipo powered no additional battery - Mode 1
 * 1273 - 6 x AA batteries in top position - Mode 2
 * 2924 - 6 x AA batteries in low position - Mode 3
 * 2868 - Mode 3 + Raspberry Pi

 **************
 *PID settings*
 *Mode 1 - P = 36; I = 0.32; D = 70
 *Mode 2 - P = ; I = ; D =
 *Mode 3 - P = 32.5; I = 0.46; D = 69.5
 ***************
 */



int acc_calibration_value = 2868;                            //Enter the accelerometer calibration value, this is the value the
                                                            //accelerometer reads when the robot is standing balanced upright.

//Various settings
float pid_p_gain = 20.5;                                     //Gain setting for the P-controller (32.5)
float pid_i_gain = 0.2;                                      //Gain setting for the I-controller (0.3)
float pid_d_gain = 40;                                       //Gain setting for the D-controller (69.5)
float turning_speed = 80;                                    //Turning speed (20)
float max_target_speed = 100;                                //Max target speed (100)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Declaring global variables
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
byte start, received_byte, low_bat;

int left_motor, throttle_left_motor, throttle_counter_left_motor, throttle_left_motor_memory;
int right_motor, throttle_right_motor, throttle_counter_right_motor, throttle_right_motor_memory;
int velocity, heartbeat_counter;

// int battery_voltage;
int receive_counter;
int gyro_pitch_data_raw, gyro_yaw_data_raw, accelerometer_data_raw;

long gyro_yaw_calibration_value, gyro_pitch_calibration_value;

unsigned long loop_timer, loop_timer2;

float angle_gyro, angle_acc, angle, self_balance_pid_setpoint;
float pid_error_temp, pid_i_mem, pid_setpoint, gyro_input, pid_output, pid_last_d_error;
float pid_output_left, pid_output_right;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Motor defines and vairables
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#define motor_left_A     2                              // Motor Left Pin A - High = forward
#define motor_left_B     4                              // Motor Left Pin B - High = reverse
#define motor_left_En    3                              // Motor Left Enable Pin, PWM on this pin will control speed
#define motor_right_A    5                              // Motor Right Pin A - High = forward
#define motor_right_B    7                              // Motor Right Pin B - High = reverse
#define motor_right_En   6                              // Motor Right Enable Pin, PWM on this pin will control speed


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Setup basic functions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void setup(){
  Serial.begin(9600);                                                       //Start the serial port at 9600 kbps
  Wire.begin();                                                             //Start the I2C bus as master
  TWBR = 12;                                                                //Set the I2C clock speed to 400kHz




  //By default the MPU-6050 sleeps. So we have to wake it up.
  Wire.beginTransmission(gyro_address);                                     //Start communication with the address found during search.
  Wire.write(0x6B);                                                         //We want to write to the PWR_MGMT_1 register (6B hex)
  Wire.write(0x00);                                                         //Set the register bits as 00000000 to activate the gyro
  Wire.endTransmission();                                                   //End the transmission with the gyro.
  //Set the full scale of the gyro to +/- 250 degrees per second
  Wire.beginTransmission(gyro_address);                                     //Start communication with the address found during search.
  Wire.write(0x1B);                                                         //We want to write to the GYRO_CONFIG register (1B hex)
  Wire.write(0x00);                                                         //Set the register bits as 00000000 (250dps full scale)
  Wire.endTransmission();                                                   //End the transmission with the gyro
  //Set the full scale of the accelerometer to +/- 4g.
  Wire.beginTransmission(gyro_address);                                     //Start communication with the address found during search.
  Wire.write(0x1C);                                                         //We want to write to the ACCEL_CONFIG register (1C hex)
  Wire.write(0x08);                                                         //Set the register bits as 00001000 (+/- 4g full scale range)
  Wire.endTransmission();                                                   //End the transmission with the gyro
  //Set some filtering to improve the raw data.
  Wire.beginTransmission(gyro_address);                                     //Start communication with the address found during search
  Wire.write(0x1A);                                                         //We want to write to the CONFIG register (1A hex)
  Wire.write(0x03);                                                         //Set the register bits as 00000011 (Set Digital Low Pass Filter to ~43Hz)
  Wire.endTransmission();                                                   //End the transmission with the gyro

  // Setup motor pins
  pinMode(motor_left_A, OUTPUT);
  pinMode(motor_left_B, OUTPUT);
  pinMode(motor_left_En, OUTPUT);
  pinMode(motor_right_A, OUTPUT);
  pinMode(motor_right_B, OUTPUT);
  pinMode(motor_right_En, OUTPUT);

  //Configure digital poort 6 as output for LED
  pinMode(13, OUTPUT);

  //Configure digital pin 12 as output for heartbeat, useful for occiloscope checking
  pinMode(12, OUTPUT);

  //Get average og 500 readings for gyro_pitch and gyro_yaw whilst robot is at rest
  for(receive_counter = 0; receive_counter < 500; receive_counter++){       //Create 500 loops
    if(receive_counter % 15 == 0)digitalWrite(13, !digitalRead(13));        //Change the state of the LED every 15 loops to make the LED blink fast
    Wire.beginTransmission(gyro_address);                                   //Start communication with the gyro
    Wire.write(0x43);                                                       //Start reading register 0x43
    Wire.endTransmission();                                                 //End the transmission
    Wire.requestFrom(gyro_address, 4);                                      //Request 4 bytes from the gyro - Gyro, X & Y out, high and low bits
    gyro_yaw_calibration_value += Wire.read()<<8|Wire.read();               //Combine the two bytes to make one integer
    gyro_pitch_calibration_value += Wire.read()<<8|Wire.read();             //Combine the two bytes to make one integer
    delayMicroseconds(3800);                                                //Wait for 3800 microseconds to simulate the main program loop time
  }
  gyro_pitch_calibration_value /= 500;                                      //Divide the total value by 500 to get the avarage gyro offset
  gyro_yaw_calibration_value /= 500;                                        //Divide the total value by 500 to get the avarage gyro offset

  loop_timer = micros() + 4000;                                             //Set the loop_timer variable at the next end loop time
  loop_timer2 = micros() + 100000;                                           //Set the loop_timer2 variable for the slow debug loop

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Main program loop
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void loop(){
  digitalWrite(12, !digitalRead(12));                                       //Heartbeat pin, useful for connection to Osciloscope
  if(Serial.available()){                                                   //If there is serial data available
    received_byte = Serial.read();                                          //Load the received serial data in the received_byte variable
    receive_counter = 0;                                                    //Reset the receive_counter variable
  }
  if(receive_counter <= 25)receive_counter ++;                              //The received byte will be valid for 25 program loops (100 milliseconds)
  else received_byte = 0x00;                                                //After 100 milliseconds the received byte is deleted


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Angle calculations
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  Wire.beginTransmission(gyro_address);                                     //Start communication with the gyro
  Wire.write(0x3F);                                                         //Start reading at register 3F
  Wire.endTransmission();                                                   //End the transmission
  Wire.requestFrom(gyro_address, 2);                                        //Request 2 bytes from the gyro
  accelerometer_data_raw = Wire.read()<<8|Wire.read();                      //Combine the two bytes to make one integer
  accelerometer_data_raw += acc_calibration_value;                          //Add the accelerometer calibration value
  if(accelerometer_data_raw > 8200)accelerometer_data_raw = 8200;           //Prevent division by zero by limiting the acc data to +/-8200;
  if(accelerometer_data_raw < -8200)accelerometer_data_raw = -8200;         //Prevent division by zero by limiting the acc data to +/-8200;

  angle_acc = asin((float)accelerometer_data_raw/8200.0)* 57.296;           //Calculate the current angle according to the accelerometer
                                                                            //vertical = 0, 90deg. = 8192. asin calculates angle in radians
                                                                            //convert to degrees by multiplying by 180/pi or 57.296

  if(start == 0 && angle_acc > -0.5 && angle_acc < 0.5){                    //If the accelerometer angle is almost 0
    angle_gyro = angle_acc;                                                 //Load the accelerometer angle in the angle_gyro variable
    start = 1;                                                              //Set the start variable to start the PID controller
  }

  Wire.beginTransmission(gyro_address);                                     //Start communication with the gyro
  Wire.write(0x43);                                                         //Start reading at register 43
  Wire.endTransmission();                                                   //End the transmission
  Wire.requestFrom(gyro_address, 4);                                        //Request 4 bytes from the gyro
  gyro_yaw_data_raw = Wire.read()<<8|Wire.read();                           //Combine the two bytes to make one integer
  gyro_pitch_data_raw = Wire.read()<<8|Wire.read();                         //Combine the two bytes to make one integer

  gyro_pitch_data_raw -= gyro_pitch_calibration_value;                      //Add the gyro calibration value
  angle_gyro += gyro_pitch_data_raw * 0.000031;                             //Calculate the traveled during this loop angle and add this to the angle_gyro variable
                                                                            //LSB/deg/s. From Data sheet, LSB = 131/deg
                                                                            //Loop time is 4000micros, therefore deg = LSB/s
                                                                            //131/0.0004 = 0.000031

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //MPU-6050 offset compensation
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Not every gyro is mounted 100% level with the axis of the robot. This can be cause by misalignments during manufacturing of the breakout board.
  //As a result the robot will not rotate at the exact same spot and start to make larger and larger circles.
  //To compensate for this behavior a VERY SMALL angle compensation is needed when the robot is rotating.
  //Try 0.0000003 or -0.0000003 first to see if there is any improvement.

  gyro_yaw_data_raw -= gyro_yaw_calibration_value;                          //Add the gyro calibration value
  //Uncomment the following line to make the compensation active
  //angle_gyro -= gyro_yaw_data_raw * 0.0000003;                            //Compensate the gyro offset when the robot is rotating

  //Complementary filter
  angle_gyro = angle_gyro * 0.9992 + angle_acc * 0.0008;                    //Correct the drift of the gyro angle with the accelerometer angle
                                                                            //The 0.9996 and 0.0004 are contants, they can be tweaked providing
                                                                            //they both add up to 1.0

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //PID controller calculations
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //The balancing robot is angle driven. First the difference between the desired angel (setpoint) and actual angle (process value)
  //is calculated. The self_balance_pid_setpoint variable is automatically changed to make sure that the robot stays balanced all the time.
  //The (pid_setpoint - pid_output * 0.015) part functions as a brake function.
  pid_error_temp = angle_gyro - self_balance_pid_setpoint - pid_setpoint;
  if(pid_output > 100 || pid_output < -100)pid_error_temp += pid_output * 0.009 ;

  pid_i_mem += pid_i_gain * pid_error_temp;                                 //Calculate the I-controller value and add it to the pid_i_mem variable
  if(pid_i_mem > 255)pid_i_mem = 255;                                       //Limit the I-controller to the maximum controller output
  else if(pid_i_mem < -255)pid_i_mem = -255;
  //Calculate the PID output value
  pid_output = pid_p_gain * pid_error_temp + pid_i_mem + pid_d_gain * (pid_error_temp - pid_last_d_error);
  if(pid_output > 255)pid_output = 255;                                     //Limit the PI-controller to the maximum controller output
  else if(pid_output < -255)pid_output = -255;

  if (pid_output > 0)pid_output = map(pid_output, 0, 255, 25, 255);
  if (pid_output < 0)pid_output = map(pid_output, 0, -255, -25, -255);

  pid_last_d_error = pid_error_temp;                                        //Store the error for the next loop

  if(pid_output < 25 && pid_output > -25)pid_output = 0;                      //Create a dead-band to stop the motors when the robot is balanced

  if(angle_gyro > 30 || angle_gyro < -30 || start == 0 || low_bat == 1){    //If the robot tips over or the start variable is zero or the battery is empty
    pid_output = 0;                                                         //Set the PID controller output to 0 so the motors stop moving
    pid_i_mem = 0;                                                          //Reset the I-controller memory
    start = 0;                                                              //Set the start variable to 0
    self_balance_pid_setpoint = 0;                                          //Reset the self_balance_pid_setpoint variable
  }

  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Control calculations
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  velocity = (received_byte & B11110000) >> 4;                              //Get the velocity from the Serial Port, stored in the upper nibble of the byte
  if(velocity > 10) velocity = 10;

  pid_output_left = pid_output;                                             //Copy the controller output to the pid_output_left variable for the left motor
  pid_output_right = pid_output;                                            //Copy the controller output to the pid_output_right variable for the right motor

  if(received_byte & B00000001){                                            //If the first bit of the receive byte is set change the left and right variable to turn the robot to the left
    pid_output_left -= velocity * 10;                                       //Increase the left motor speed
    pid_output_right += velocity * 10;                                      //Decrease the right motor speed
  }
  if(received_byte & B00000010){                                            //If the second bit of the receive byte is set change the left and right variable to turn the robot to the right
    pid_output_left +=   velocity * 10;                                     //Decrease the left motor speed
    pid_output_right -= velocity * 10;                                      //Increase the right motor speed
  }

  if(received_byte & B00000100){                                            //If the third bit of the receive byte is set change pid_setpoint to move forward
    pid_setpoint = velocity * -0.38;
//  if(pid_setpoint > -2.5)pid_setpoint = -velocity * 1.5;                  //Set pid_setpoint to the velocity * 1.5,  1.5 just gives a maximum lean angle
//  if(pid_output > max_target_speed * -1)pid_setpoint -= 0.005;          //Slowly change the setpoint angle so the robot starts leaning forewards
  }
  if(received_byte & B00001000){                                            //If the forth bit of the receive byte is set change the pid_setpoint to move backwards
    pid_setpoint = velocity * 0.38;
//  if(pid_setpoint < 2.5)pid_setpoint = velocity * 1.5;                    //Set pid_setpoint to the velocity * 1.5,  1.5 just gives a maximum lean angle
//  if(pid_output < max_target_speed)pid_setpoint += 0.005;                 //Slowly change the setpoint angle so the robot starts leaning backwards
  }

  if(!(received_byte & B00001100)){                                         //Slowly reduce the setpoint to zero if no foreward or backward command is given
    if(pid_setpoint > 0.5)pid_setpoint -=0.05;                              //If the PID setpoint is larger then 0.5 reduce the setpoint with 0.05 every loop
    else if(pid_setpoint < -0.5)pid_setpoint +=0.05;                        //If the PID setpoint is smaller then -0.5 increase the setpoint with 0.05 every loop
    else pid_setpoint = 0;                                                  //If the PID setpoint is smaller then 0.5 or larger then -0.5 set the setpoint to 0
  }

  //The self balancing point is adjusted when there is not forward or backwards movement from the transmitter. This way the robot will always find it's balancing point
  if(pid_setpoint == 0){                                                    //If the setpoint is zero degrees
    if(pid_output < 0)self_balance_pid_setpoint += 0.0015;                  //Increase the self_balance_pid_setpoint if the robot is still moving forewards
    if(pid_output > 0)self_balance_pid_setpoint -= 0.0015;                  //Decrease the self_balance_pid_setpoint if the robot is still moving backwards
  }


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Motor pulse calculations
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
  //To compensate for the non-linear behaviour of the stepper motors the folowing calculations are needed to get a linear speed behaviour.
  if(pid_output_left > 0)pid_output_left = 405 - (1/(pid_output_left + 9)) * 5500;
  else if(pid_output_left < 0)pid_output_left = -405 - (1/(pid_output_left - 9)) * 5500;

  if(pid_output_right > 0)pid_output_right = 405 - (1/(pid_output_right + 9)) * 5500;
  else if(pid_output_right < 0)pid_output_right = -405 - (1/(pid_output_right - 9)) * 5500;
*/
  //Calculate the needed pulse time for the left and right stepper motor controllers
  if(pid_output_left > 0)left_motor = pid_output_left;
  else if(pid_output_left < 0)left_motor = pid_output_left;
  else left_motor = 0;

  if(pid_output_right > 0)right_motor = pid_output_right;
  else if(pid_output_right < 0)right_motor = pid_output_right;
  else right_motor = 0;

  // Copy the pulse time to the throttle variables so the interrupt subroutine can use them
  throttle_left_motor = left_motor;
  throttle_right_motor = right_motor;

  // Set motor speeds
  LeftMotorRun(throttle_left_motor);
  RightMotorRun(throttle_right_motor);


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Loop time timer
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //The angle calculations are tuned for a loop time of 4 milliseconds. To make sure every loop is exactly 4 milliseconds a wait loop
  //is created by setting the loop_timer variable to +4000 microseconds every loop.
  while(loop_timer > micros());
  loop_timer += 4000;



  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  //Loop time timer - using loop_timer2 variable
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
  // Secondary slow loop for debugging
  // Main loop runs at 250 Hz or once every 0.004 seconds or every 4000us
  // Lets run the debug loop at 10 Hz or once every 0.1 seconds or every 10,0000us
  if (loop_timer2 < micros()){
    loop_timer2 += 100000;
   // Serial.print(pid_output_left);       //the first variable for plotting
   // Serial.print(",");              //seperator
   // Serial.print(pid_output_right);       //the first variable for plotting
   // Serial.print(",");              //seperator
    Serial.print(pid_error_temp);       //the first variable for plotting
    Serial.print(",");              //seperator
    Serial.println(velocity);      //the second variable for plotting
  

    
    // Heartbeat LED to show we are still running
    heartbeat_counter +=1;
    if (heartbeat_counter > 15){
      digitalWrite(13, !digitalRead(13));  
      heartbeat_counter = 0;
    }
  }  //End slow debug loop
  
}  //End main loop


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void LeftMotorFwd(int speed){
  digitalWrite(motor_left_A, HIGH);
  digitalWrite(motor_left_B, LOW);
  analogWrite(motor_left_En, speed);
}

void LeftMotorRev(int speed){
  digitalWrite(motor_left_A, LOW);
  digitalWrite(motor_left_B, HIGH);
  analogWrite(motor_left_En, speed);
}

void RightMotorFwd(int speed){
  digitalWrite(motor_right_A, HIGH);
  digitalWrite(motor_right_B, LOW);
  analogWrite(motor_right_En, speed);
}

void RightMotorRev(int speed){
  digitalWrite(motor_right_A, LOW);
  digitalWrite(motor_right_B, HIGH);
  analogWrite(motor_right_En, speed);
}

void AllMotorsFwd(int speed){
  LeftMotorFwd(speed);
  RightMotorFwd(speed);
}

void AllMotorsRev(int speed){
  LeftMotorRev(speed);
  RightMotorRev(speed);
}

void LeftMotorStop(void){
  digitalWrite(motor_left_A, LOW);
  digitalWrite(motor_left_B, LOW);
  digitalWrite(motor_left_En, LOW);
}

void RightMotorStop(void){
  digitalWrite(motor_right_A, LOW);
  digitalWrite(motor_right_B, LOW);
  digitalWrite(motor_right_En, LOW);
}

void AllMotorsStop(void){
  LeftMotorStop();
  RightMotorStop();
}

void LeftMotorRun(int speed){
  if (speed > 0){               //Forward
    LeftMotorFwd(speed);
  }

  else if (speed < 0){          //Reverse
    LeftMotorRev(speed * -1);
  }

  else {
    LeftMotorStop();            //Stop
  }

}

void RightMotorRun(int speed){
  if (speed > 0){               //Forward
    RightMotorFwd(speed);
  }

  else if (speed < 0){
    RightMotorRev(speed * -1);   //Reverse
  }

  else {
    RightMotorStop();           //Stop
  }
}
