/*
BalanceBot Arduino code, BalanceBot is an inverted pendulum robot powered by DC
motors and keeps itself upright.
Motor Test Code
Drive Motors with an Arduino Mini Pro using an L239D 'H' bridge
Copyright (C) 2018  Keith Ellis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


#define motor_left_A     2                              // Motor Left Pin A - High = forward
#define motor_left_B     4                              // Motor Left Pin B - High = reverse
#define motor_left_En    3                              // Motor Left Enable Pin, PWM on this pin will control speed
#define motor_right_A    5                              // Motor Right Pin A - High = forward
#define motor_right_B    7                              // Motor Right Pin B - High = reverse
#define motor_right_En   6                              // Motor Right Enable Pin, PWM on this pin will control speed

void setup() {

  Serial.begin(9600);

  pinMode(motor_left_A, OUTPUT);
  pinMode(motor_left_B, OUTPUT);
  pinMode(motor_left_En, OUTPUT);
  pinMode(motor_right_A, OUTPUT);
  pinMode(motor_right_B, OUTPUT);
  pinMode(motor_right_En, OUTPUT);
}

void loop() {

  AllMotorsRev(46);

}

void LeftMotorFwd(int speed){
  digitalWrite(motor_left_A, HIGH);
  digitalWrite(motor_left_B, LOW);
  analogWrite(motor_left_En, speed);
}

void LeftMotorRev(int speed){
  digitalWrite(motor_left_A, LOW);
  digitalWrite(motor_left_B, HIGH);
  analogWrite(motor_left_En, speed);
}

void RightMotorFwd(int speed){
  digitalWrite(motor_right_A, HIGH);
  digitalWrite(motor_right_B, LOW);
  analogWrite(motor_right_En, speed);
}

void RightMotorRev(int speed){
  digitalWrite(motor_right_A, LOW);
  digitalWrite(motor_right_B, HIGH);
  analogWrite(motor_right_En, speed);
}

void AllMotorsFwd(int speed){
  LeftMotorFwd(speed);
  RightMotorFwd(speed);
}

void AllMotorsRev(int speed){
  LeftMotorRev(speed);
  RightMotorRev(speed);
}

void LeftMotorStop(void){
  digitalWrite(motor_left_A, LOW);
  digitalWrite(motor_left_B, LOW);
  digitalWrite(motor_left_En, LOW);
}

void RightMotorStop(void){
  digitalWrite(motor_right_A, LOW);
  digitalWrite(motor_right_B, LOW);
  digitalWrite(motor_right_En, LOW);
}

void AllMotorsStop(void){
  LeftMotorStop();
  RightMotorStop();
}
