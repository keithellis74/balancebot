'''
BalanceBot Arduino code, BalanceBot is an inverted pendulum robot powered by DC
motors and keeps itself upright.
Test serial UART code
Copyright (C) 2018  Keith Ellis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


#! /home/pi/venv/bin/python
import time
import serial

ser = serial.Serial(
	port='/dev/ttyS0',
	baudrate = 9600,
	parity = serial.PARITY_NONE,
	stopbits = serial.STOPBITS_ONE,
	bytesize = serial.EIGHTBITS,
	timeout = 1
	)

couter = 0

while True:
	var = input("Enter test text now: ")
	x = ser.readline()
	print(x)
