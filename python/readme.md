# Raspberry Pi setup to use serial UART port on Raspberry Pi

## Setup Raspberry Pi

* ```sudo raspi-config```
* Select interface options / Serial
* Disable console over serial and enable hardware serial over GPIO

## Setup python environment

### Setup virtual environment


* ```sudo apt-get install python3-venv```
* ```python3 -m venv venv```

Active virtual environemnt

* ```source ~/venv/bin/activate```

## Dependencies

* Make sure the virtual environment is active

The virtual environment does not have any modules installed so we need to install the following

* RPi.gpio - Base GPIO library, gpiozero needs this
* gpiozero - gpio zero library
* pyserial - serial library
* approxengineering input library - required for Rock Candy controller 


do so using:

```pip install rpi.gpio gpiozero pyserial```

```pip install approxeng.input```

## Documents

[pyserial documentation](https://pythonhosted.org/pyserial/pyserial_api.html)

[approxamte engineering input library](https://approxeng.github.io/approxeng.input/index.html#)

