'''
BalanceBot Arduino code, BalanceBot is an inverted pendulum robot powered by DC
motors and keeps itself upright.
Controller code to run on a Raspberry Pi connected to the Arduino over the serial
UART
Copyright (C) 2018  Keith Ellis

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''


#! /home/pi/venv/bin/python
from time import sleep
import serial
from approxeng.input.selectbinder import ControllerResource
from gpiozero import LED, PWMLED
from struct import pack
import os

# Setup the serial port
ser = serial.Serial(port='/dev/ttyS0',
		baudrate = 9600,
		parity = serial.PARITY_NONE,
		stopbits = serial.STOPBITS_ONE,
		bytesize = serial.EIGHTBITS,
		timeout = 1
		)

# Setup LED's
green_led = LED(6)		# Green LED is on whilst programme is running
red_led = PWMLED(5)		# Red LED pulses whilst the 'Exit' or 'Shutdown' buttons are being pressed

first_press = True 		# Variable to allow LED to blink whilst buttons are being held.

running = True  		# Programme runs as long as this is True
green_led.on()			# We are running, turn the green LED on


# Main loop
while running:
	try:
		with ControllerResource() as joystick:
			print('Found a joystick and connected')
			while joystick.connected and running:

###############################
# sendByte code
#
# 00000000 - Do nothing
# 00000001 - Turn left
# 00000010 - Turn right
# 00000100 - Move forwards
# 00001000 - Move backwards

#### Proportionl Control

# As well as sending the commands above which are basically directions
# we shall now send the velocity read from the stick in the high byte
# We shall multiply the velocity from the stick by 10.
# For example
# Full speed forward shall be 1 * 10 = 10, convert into BIN = 1010
# Bit shift this to the high byte and OR with the direction, therefore:
# the command shall be 0b10100100

################################

				sendByte = 0b00000000
				# Check and react to joystick input
				lx, ly , exit, quit = joystick['lx','ly', 'select', 'start']

				if lx < 0:								  # turn left
					turn_speed = int(lx * -10) << 4		  # Convert turn_speed into a value between 0 & 10 and send to high nibble
					sendByte |= (0b00000001 | turn_speed) # Bitwise OR with turn_speed then OR= with sendbyte to combine into a single bytes

				if lx > 0:								  # turn right
					turn_speed = int(lx * 10) << 4		  # Convert turn_speed into a value between 0 & 10 and send to high nibble
					sendByte |= (0b00000010 | turn_speed) # Bitwise OR with turn_speed then OR= with sendbyte to combine into a single bytes

				if ly > 0:								  # Move Forwards
					speed = int(ly * 10) << 4  			  # Convert velovity into a value between 0 & 10 and send to high nibble
					sendByte |= (0b00000100 | speed)	  # Bitwise OR with speed then OR= with sendbyte to combine into a single bytes

				if ly < 0:								  # move backwards
					speed = int(ly * -10) <<4			  # Convert velovity into a value between 0 & 10 and send to high nibble
					sendByte |= (0b00001000 | speed)	  # Bitwise OR with speed then OR= with sendbyte to combine into a single bytes

				if exit is not None:
					if first_press == True:
						red_led.pulse(fade_in_time=0.5, fade_out_time = 0.5)
						first_press = False
					if exit  > 5:
						green_led.off()
						running = False

				if quit is not None:
					if first_press == True:
						red_led.pulse(fade_in_time=0.2, fade_out_time = 0.2)
						first_press = False
					if quit > 8:
						green_led.off()
						running = False
						os.system( "sudo poweroff" )

				if quit is None and exit is None:
					red_led.off()
					first_press = True

				# Send the command to the serial port
				val = pack("B", sendByte)
				ser.write(val)
				print(val)
				sleep(0.04)

		# Joystick disconnected...
		print('Connection to joystick lost')
	except IOError:
		# No joystick found, wait for a bit before trying again
		print('Unable to find any joysticks')
		sleep(1.0)
